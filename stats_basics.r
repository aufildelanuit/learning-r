#!/usr/bin/env Rscript


#######################################################
#
# This file can be run in RStudio, or with:
#     Rscript stats_basics.r
# Or inside the R command line with:
#     source('stats_basics.r')
# Or on linux or a virtual machine with:
#     ./stats_basics.r
# ... from the directory where the file is located
#
#######################################################


### assign a value to a variable
a <- 25
### also possible this way
b = 4


### display variables
print(paste0("a = ", a, "; b = ", b))
### also works this way
a
b
### or this way
cat(c(a, b, "\n\n"))  ### '\n' stands for "new line"
### notice that there is a slight difference in the way 
### the output is displayed between print() and cat()


### define a function:
# name <- function(<variables>) (<definition>)
### if the function returns a value, or:
# name <- function(<variables>) {<definition>}
### if the function doesn't necessarily return anything
###
### the following repeats the chatacter '#' 50 times
### and prints it followed by a newline
separator <- function() {  ### no variables this time
    cat(c(strrep('#', 50), '\n'))
}  ### and no specific return value, we just want to print
#
### another function that performs a calculation
multiply <- function(a, b) (a * b)
### or:
multiply <- function(a, b) {return (a*b)}
### the notation with { } is most often used when more than
### one line of code is needed in the function definition.
### If there is only one line (i.e. one calculation)
### then the line following function() will be the return value.
### then call the function or method
print("2*12 is... ")
cat(c(
    multiply(2, 12),
    '\n\n' ### for readability
    )
)
#
separator()


### create a data frame with named variables
### Note that there is no strict rule on indentation
d <- data.frame(
    x = c(1, 5, 8, 4),  ### notice the comma
    y = c(2, 6, 4, 8)
)
### also notice that c(...) is needed to make series 
### of numbers, and that name declaration inside of
### the function is done using "=" instead of "<-"
print("data frame (d):")
print(d)
cat('\n\n')


###
separator()

### get basic information about a data frame
print("summary of d:")
summary(d)
### add a newline or two for readability
cat('\n\n')
#
separator()
#
print("structure of d:")
str(d)
#
cat('\n\n')


###
separator()

### refer to one series (element) in the data frame
print("values of x in d:")
cat(d$x)
### newline for clarity
cat('\n\n')


###
separator()

### add a column (series) to the data frame
print("adding a column to d:")
d$price <- c(128, 145, 161, 192)
#
str(d)
cat('\n\n')


###
separator()

### mean and standard deviation 
print("mean(x):")
cat(mean(d$x))
cat('\n')
#
print("std(x):")
cat(sd(d$x))
cat('\n\n\n')


###
separator()

### covariance and correlation
print("covariance(x, y):")
cat(cov(d$x, d$y))
cat('\n')
#
print("correlation(x, y):")
cat(cor(d$x, d$y))
cat('\n\n')


###
separator()

### binomial test
print("A binomial test [8 in 15, Pr(success)=0.2]:")
binom.test(
    8, 
    15, 
    p=0.2, 
    alternative="two.sided",
    conf.level=0.95 
)
cat('\n\n')


###
separator()

### z-test on proportions (Normal distribution)
print("A z-test [79 / 100 when expected 85%]:")
prop.test(79, 100, p=0.85)
cat('\n\n')


###
separator()

### t-test against a theoretical mean
print("A t-test [d$x, theoretical mean=5]:")
t.test(d$x, mu=5, conf.level=0.95)
cat('\n\n')


###
separator()

### t-test between two unpaired samples
print("A t-test [d$x, d$y, unpaired]:")
t.test(d$x, d$y)
cat('\n\n')


###
separator()

### t-test between two paired samples (repeated measures)
print("A t-test [d$x, d$y, paired]:")
t.test(d$x, d$y, paired=TRUE)
cat('\n\n')


###
separator()

### linear model fitting and ANOVA
print("ANOVA (F-test) on price ~ A*x + B*y + const. :")
model <- lm(price ~ x+y, data=d)
summary(model)
cat('\n\n')


###
separator()

### accessing the coefficients
print("Regression coefficients:")
cat(c(
    "Intercept: ", 
    model$coefficients['(Intercept)'], 
    '\n'
))
#
cat(c(
    "x: ",
    model$coefficients['x'],
    '\n'
))
#
cat(c(
    "y: ",
    model$coefficients['y'],
    '\n'
))
cat('\n\n')


###
separator()

### two ways for data prediction from model
print("Data prediction [x=4, y=8]:")
cat("Method 1: ")
cat(c(
    predict(model, newdata=data.frame(x=4, y=8)),
    '\n'
# or: predict(model, newdata=list(x=4, y=8))
))
#
prediction <- function(x, y, ...) {
    ### this piece of code will make the model argument 
    ### optional. We can use the function without it,
    ### but if we want, we can change the model 
    ### with model=something in the function arguments
    ### However this is not mandatory...
    ### The default for model is "model" as defined earlier
    params <- list(...)
    if(!("model" %in% names(params))) {
        params$model <- model
    }
    ### Here is the essential part (this and following)
    c <- params$model$coefficients
    ### The return value is as follows:
    return(c['(Intercept)'] + c['x']*x + c['y']*y)
}
cat("Method 2: ")
cat(c(
    prediction(4, 8),
    '\n\n\n'
))


###
separator()

### Linear regression using matrices
print("Linear regression using matrices:")
#
### define a matrix as follows:
X <- as.matrix(cbind(rep(1, length(d$x)), d$x, d$y))
### alternatively:
#X <- matrix(c(rep(1, length(d$x)), d$x, d$y), ncol=3)
#
### matrix multiplication is %*%
### matrix inverse is solve()
### matrix transpose is t()
C <- solve(t(X) %*% X)
Cjj <- diag(C)
H <- X %*% C %*% t(X)
#
B <- C %*% t(X) %*% as.matrix(d$price)
cat(c("Regression coefficients: ", '\n'))
cat(c(B, '\n\n'))
#
y <- as.matrix(d$price)
y_pred <- H %*% y
SSE <- as.numeric(t(y) %*% y - t(y_pred) %*% y)
SSR <- as.numeric(t(y_pred) %*% y - (sum(y)**2)/length(y))
SST <- SSR + SSE
VAR <- as.numeric(SSE / (length(y) - ncol(X)))
COV_B <- VAR * C
residuals <- y - y_pred
#
coef_pval <- 2 * pt(
    B/sqrt(VAR*Cjj), 
    length(y)-ncol(X), 
    lower.tail=FALSE
)
cat(c("two-sided p-value for the coefficients [t-test]:", '\n'))
cat(c(as.numeric(coef_pval), '\n\n'))
#
R2 <- SSR/SST
cat("R^2: ")
cat(c(R2, '\n\n'))


